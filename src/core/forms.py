from django import forms
from django.utils.translation import gettext as _

from .models import Link


class LinkForm(forms.ModelForm):
    """Form for link model."""

    link = forms.URLField(label=_("Link"), max_length=200, required=True)
    alias = forms.CharField(
        label=_("Alias"),
        max_length=30,
        required=False,
        help_text=_("The field can be left empty, it will be generated automatically."),
    )

    class Meta:
        model = Link
        fields = ("link", "alias")
