import hashlib
import os

from django.db import models
from django.urls import reverse


class Link(models.Model):
    """
    Stores a link model.
        - link: link to web page.
        - alias: link shortening (generated automatically or set by the user).
        - created_at: date when model was created.
        - counter: the number of clicks on the shortened link.
    """

    link = models.URLField(max_length=200)
    alias = models.CharField(max_length=30, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    counter = models.PositiveIntegerField(default=0)

    def set_alias(self, alias):
        """Changes the value of alias field."""
        self.alias = alias

    def __str__(self):
        """Returns a string representation of link model."""
        return self.alias

    def get_absolute_url(self):
        """Return absolute link for model."""
        return reverse("link_detail", args=[str(self.alias)])

    def save(self, *args, **kwargs):
        """
        Save model to database.
        If the alias value is not set, a hash of the link is generated and set as an alias.
        """
        if not self.alias:
            salt = os.urandom(32)
            key = hashlib.pbkdf2_hmac("sha256", self.link.encode("utf-8"), salt, 100000, dklen=128)
            key = key.hex()[:15]

            while Link.objects.filter(alias=key).exists():
                salt = os.urandom(32)
                key = hashlib.pbkdf2_hmac("sha256", self.link.encode("utf-8"), salt, 100000, dklen=128)
                key = key.hex()[:15]

            self.set_alias(key)
        super(Link, self).save(*args, **kwargs)
