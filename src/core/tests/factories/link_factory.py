import factory
from factory.django import DjangoModelFactory

from core.models import Link


class LinkFactory(DjangoModelFactory):
    """Factory for link model - generate fake data."""

    link = factory.Faker("uri")

    class Meta:
        model = Link
