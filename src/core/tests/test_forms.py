import pytest
from django.utils.crypto import get_random_string
from faker import Faker

from ..forms import LinkForm
from ..models import Link


@pytest.fixture
def fake():
    return Faker()


@pytest.mark.django_db
class TestLinkForm:
    def test_link_form_with_alias_success(self, fake):
        """Test for form with alias successfully."""
        url = fake.uri()
        alias = fake.word()

        data = {"link": url, "alias": alias}

        form = LinkForm(data=data)
        assert form.is_valid()

        form.save()

        assert Link.objects.filter(alias=alias).count() == 1
        assert Link.objects.get(alias=alias).counter == 0

    def test_link_form_without_alias_success(self, fake):
        """Test link form without alias."""
        url = fake.uri()

        data = {
            "link": url,
        }

        form = LinkForm(data=data)
        assert form.is_valid()

        link = form.save()

        assert Link.objects.filter(alias=link.alias).count() == 1
        assert Link.objects.get(alias=link.alias).counter == 0

    def test_link_form_with_existing_alias(self, fake):
        """Test link form with alias, that exist."""
        url = fake.uri()
        url_2 = fake.uri()
        alias = fake.word()

        data = {"link": url_2, "alias": alias}

        Link.objects.create(link=url, alias=alias)

        form = LinkForm(data=data)

        is_valid = form.is_valid()

        assert not is_valid

    def test_link_form_with_long_url(self, fake):
        """Test form with link length > 200."""
        url_domain = fake.url()
        url_addition = get_random_string(length=199)
        url = f"{url_domain}{url_addition}"

        data = {
            "link": url,
        }

        form = LinkForm(data=data)

        is_valid = form.is_valid()

        assert not is_valid

    def test_link_form_with_long_alias(self, fake):
        """Test form with alias length > 30."""
        url = fake.uri()
        alias = get_random_string(length=31)

        data = {"link": url, "alias": alias}

        form = LinkForm(data=data)

        is_valid = form.is_valid()

        assert not is_valid

    def test_link_form_with_empty_data(self):
        """Empty form."""
        form = LinkForm()

        is_valid = form.is_valid()

        assert not is_valid

    def test_link_form_without_url(self):
        """Link form with empty link."""

        alias = get_random_string(10)

        data = {
            "alias": alias,
        }

        form = LinkForm(data)
        is_valid = form.is_valid()

        assert not is_valid
