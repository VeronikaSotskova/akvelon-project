from random import randint

import pytest
from django.test import Client
from django.urls import reverse
from django.utils.crypto import get_random_string
from faker import Faker

from .factories.link_factory import LinkFactory


@pytest.fixture
def client():
    return Client()


@pytest.mark.django_db
class TestLinkViews:
    def test_link_detail_success(self, client):
        """Check that detail view redirect to link."""
        link = LinkFactory.create()
        response = client.get(reverse("link_detail", args=[str(link.alias)]))
        assert 300 <= response.status_code <= 399
        assert response.url == link.link

    def test_link_detail_fail(self, client):
        """Check detail view with nonexistent alias."""
        response = client.get(reverse("link_detail", args=[str(get_random_string(5))]))
        assert response.status_code == 404

    def test_link_detail_check_if_counter_increases(self, client):
        """Check that counter increase if someone follow the link."""
        link = LinkFactory.create()

        assert link.counter == 0

        count_of_following_link = randint(1, 8)

        for _ in range(count_of_following_link):
            client.get(reverse("link_detail", args=[str(link.alias)]))
        link.refresh_from_db()

        assert count_of_following_link == link.counter

    def test_link_list_view(self, client):
        """Check that list view return 200 status code, render template and return equals links."""
        count_of_links = randint(1, 5)
        links = []
        for _ in range(count_of_links):
            links.append(LinkFactory.create())

        response = client.get(reverse("link_list"))

        assert response.status_code == 200
        assert "link/list.html" in response.template_name
        assert set(response.context["links"]) == set(links)

    def test_link_create_view(self, client):
        """Checks if link appears in context, when it created."""
        fake = Faker()
        link = fake.uri()

        response = client.post(reverse("link_create"), data={"link": link})

        assert response.context["link"]
        assert response.status_code == 200
