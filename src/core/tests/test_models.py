import pytest
from django.db import IntegrityError
from django.utils.crypto import get_random_string

from .factories.link_factory import LinkFactory


@pytest.mark.django_db
class TestLinkModel:
    def test_create_model_success(self):
        """Test that link have all fields."""
        link = LinkFactory.create()
        link.save()

        assert link.created_at
        assert link.alias
        assert link.counter == 0
        assert link.link

    def test_create_model_fail(self):
        """Test create link with alias that have in database."""
        link_1 = LinkFactory.create()

        link_2 = LinkFactory.create()
        link_2.set_alias(link_1.alias)
        with pytest.raises(IntegrityError):
            link_2.save()

    def test_set_alias_with_random_alias(self):
        """Test that alias set correctly."""
        link = LinkFactory.create()
        alias = get_random_string(length=15)
        link.set_alias(alias)

        assert alias == link.alias

    def test_str_function(self):
        """Test that __str__ function return alias."""
        link = LinkFactory.create()

        assert link.alias == str(link)

    def test_get_absolute_url(self):
        """Test that url return alias."""
        link = LinkFactory.create()

        assert link.get_absolute_url() == f"/{link.alias}"
