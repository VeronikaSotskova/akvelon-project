from django.urls import path

from .views import LinkCreateView, LinkDeleteView, LinkDetailView, LinkListView

urlpatterns = [
    path("", LinkCreateView.as_view(), name="link_create"),
    path("<int:pk>/delete/", LinkDeleteView.as_view(), name="link_delete"),
    path("list/", LinkListView.as_view(), name="link_list"),
    path("<alias>", LinkDetailView.as_view(), name="link_detail"),
]
