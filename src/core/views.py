from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, DeleteView, ListView
from django.views.generic.detail import SingleObjectMixin

from .forms import LinkForm
from .models import Link


class LinkDetailView(SingleObjectMixin, View):
    """
    View for redirect to original link.
    """

    model = Link
    slug_field = "alias"
    slug_url_kwarg = "alias"

    def get(self, request, *args, **kwargs):
        """Increase the counter of link."""
        self.object = self.get_object()
        self.object.counter += 1
        self.object.save()

        return redirect(self.object.link)


class LinkListView(ListView):
    """View for representation all links."""

    model = Link
    template_name = "link/list.html"
    context_object_name = "links"
    paginate_by = 7

    def get_queryset(self):
        """Links are sorted by the number of clicks, from more to less, then by date of creation, then by alias."""
        queryset = Link.objects.order_by("-counter", "-created_at", "alias")
        return queryset


class LinkCreateView(CreateView):
    """View for create link."""

    model = Link
    form_class = LinkForm
    template_name = "link/create.html"

    def form_valid(self, form):
        """When the link is created, it is rendered on the page."""
        super(LinkCreateView, self).form_valid(form)
        return render(self.request, self.template_name, self.get_context_data(form=form))


class LinkDeleteView(DeleteView):
    """View for delete link. When link is deleted, it redirect to list of links."""

    model = Link
    template_name = "link/delete.html"
    success_url = reverse_lazy("link_list")
