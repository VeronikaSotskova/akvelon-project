# TINY URL

### Create .env file - example located in
```shell
/src/.env.example
```

### Install poetry
```shell
pip install poetry
```

### Build image, create and start database container
```shell
docker-compose up -d --build
```

### Install the project dependencies
```shell
cd src && poetry install
```

### Spawn a shell within the virtual environment
```shell
poetry shell
```

### Apply migrations
```shell
python manage.py migrate
```

### Create superuser
```shell
python manage.py createsuperuser
```

### Start app
```shell
python manage.py runserver
```

### For coverage
```shell
coverage run -m pytest
coverage report -m
```

